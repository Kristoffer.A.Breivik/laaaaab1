package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	// Google.com/Stackoverflow tips
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        System.out.println("Let's play round " + roundCounter);

        Random rand = new Random();
        String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        
        String humanChoice;
        
        
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            humanChoice = sc.nextLine();
            humanChoice = humanChoice.toLowerCase();
            if( rpsChoices.contains(humanChoice)){
                winnner(humanChoice, computerChoice);
                System.out.println("Score: human "+ humanScore + ", computer " + computerScore);
            
                System.out.println("Do you wish to continue playing? (y/n)?");
                String answer = sc.nextLine();
                if (answer.equals("y")){
                    roundCounter ++;
                run();
                }
                else {
                    System.out.println("Bye bye :)");
                }
            }
                
            
            else { 
        System.out.println("I do not understand " + humanChoice + ". Could you try again?");
        run();   }
        }
    
    public void winnner(String humanChoice, String computerChoice){
        if (humanChoice.equals(computerChoice)){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". It's a tie!");
        }else if (humanChoice.equals("rock") && computerChoice.equals("scissors")){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!"); 
            humanScore ++; 
        }else if (humanChoice.equals("scissors") && computerChoice.equals("rock")){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
            computerScore ++;
        }else if (humanChoice.equals("rock") && computerChoice.equals("paper")){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
            computerScore ++;
        }else if (humanChoice.equals("paper") && computerChoice.equals("rock")){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
            humanScore ++;
        }else if (humanChoice.equals("paper") && computerChoice.equals("scissors")){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
            computerScore ++; 
        }else if (humanChoice.equals("scissors") && computerChoice.equals("paper")){
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
            humanScore ++;       
        }
        }
    


        


    


                

        
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        
        return userInput;
    }

}
